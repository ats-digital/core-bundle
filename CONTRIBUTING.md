### Coding Style
---
* coding standards are based on the [PSR-1](https://www.php-fig.org/psr/psr-1/), [PSR-2](https://www.php-fig.org/psr/psr-2/) and [PSR-4](https://www.php-fig.org/psr/psr-4/) standards. This will be enfoced by the CI pipleine
* Code structure should conform with [Symfony's recommendations](https://symfony.com/doc/3.4/contributing/code/standards.html#structure)
* Symfony's [naming convention](https://symfony.com/doc/3.4/contributing/code/standards.html#naming-conventions) should be used
* Doc blocks should conform with [Symfony's recommendations](https://symfony.com/doc/3.4/contributing/code/standards.html#documentation)


### QA & Tests
---
* PHPCS must be run with utmost severity level
* PHPSTAN must be run with utmost severity level
* Projets should aim for a code coverage of at least `80%`
* Projects should aim to `Platinum` level on SensoInsight checks. Below `Silver` level is considered a failre
* No security vunerability should be reported in the composer.lock

### Git workflow
---
- Development efforts should be made on the features branches
- Once a feature is completed (or bug fixed), a merge request shall be creataed.
- The features branches are merged in the `master` branch by the projet's maintainer and the feature branch is closed
- Release branches are created per version (e.g. v1.x) and are kept alive.
- Hot fixes & release maintenance are forked from each release branch
- the master branch shall have the most capable version


### Backward Compatibility policy
---
* Backward compatibility must be insured between minor versions
* Major version upgrade can make breaking changes

### Versioning
* Semantic versioning[(1)](https://semver.org/) [(2)](https://datasift.github.io/gitflow/Versioning.html) shall be used

### Issues & Features requests
---
- All bug reports and feature requests should be created in the projets GitLab Board
