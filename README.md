### General
---
Core technical features, base for other ATS bundles

### Features & Capabilities
---

- CSVResponse type:
    - ✔️ Provides wrapper ontop of the Symfony's Response object to handle CSV downloads

- Commands:
    - ✔️ Command events subscriber
    - ✔️ Doctrine MongoDB Bad References Check Command

- Managers:
    - AbstractBaseManager for pattern enforcement

- Services & Utilities:
    - ️✔ String Wrapper/Helper class

### Setup
---

1- Update your composer.json to add ATS private packagist:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://packagist.ats-digital.com"
        }
    ],
}
```
2- Install the bundle in your application
```sh
$ php composer require ats/core-bundle
```

3- Register the bundle in the AppKernel
```php
<?php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new ATS\CoreBundle\ATSCoreBundle(),
            // ...
        ];
    }
    // ...
}
```

### Configuration
---
N/A

### HOWTO:
---
Q: How to enable MongoDB full text Search ?

A:

- Annotate the Document class with
```php
    /**
     * @ODM\Document()
     * @ODM\Index(keys={"username"="text"})
     * ...
     */
```
- Update the database indexes


#### What's new?
[CHANGELOG.md](CHANGELOG)
