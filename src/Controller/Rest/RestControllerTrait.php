<?php declare (strict_types = 1);

namespace ATS\CoreBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * BaseRestController
 *
 * @author Ali TURKI <aturki@ats-digital.com>
 *
 * @property ContainerInterface $container
 */
trait RestControllerTrait
{

    /**
     * Renders Response
     *
     * @param mixed $data
     * @param array $groups
     * @param int $responseCode
     *
     * @return JsonResponse
     */
    protected function renderResponse($data, array $groups = [], int $responseCode = Response::HTTP_OK): Response
    {
        $response = null;
        $context = ['enable_max_depth' => true];

        /** @var SerializerInterface $serializer */
        $serializer = $this->container->get('serializer');

        if (count($groups) > 0) {
            $context['groups'] = $groups;
        }

        try {
            $jsonData = $serializer->serialize(
                $data,
                'json',
                $context
            );

            $response = new JsonResponse($jsonData, $responseCode, [], true);
        } catch (\Exception $e) {
            $error = [
                'message' => $e->getMessage(),
            ];

            $response = new JsonResponse(
                json_encode($error),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $response;
    }
}
