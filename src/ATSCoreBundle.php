<?php declare(strict_types=1);

namespace ATS\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * ATSCoreBundle
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ATSCoreBundle extends bundle
{
}
