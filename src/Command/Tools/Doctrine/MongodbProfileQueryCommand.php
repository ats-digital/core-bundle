<?php declare (strict_types = 1);
namespace ATS\CoreBundle\Command\Tools\Doctrine;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\InputOption;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * @author Ali Turki <aturki@ats-digital.com>
 */
class MongodbProfileQueryCommand extends ContainerAwareCommand
{
    const CMD_NAME = 'ats:core:tools:doctrine:mongodb:profile';

    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setName(self::CMD_NAME)
            ->setDescription("Reports timing of all DocumentRepository methods")
            ->addOption('collection', null, InputOption::VALUE_REQUIRED, 'Mongo collection to work with')
            ->addOption('slow-only', null, InputOption::VALUE_NONE, 'Report only slow queries')
            ->addOption('threshold', null, InputOption::VALUE_REQUIRED, 'Threshold for slow queries', 20)
            ->addOption('report-errors', 'r', InputOption::VALUE_NONE, 'Display errors during tests execution');
    }

    /**
     * {@inheritDoc}
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $documentsMetadata = [];
        $testSet = [];

        /** @var bool $reportErrors */
        $reportErrors = $input->getOption('report-errors');
        /** @var bool $onlySlow */
        $onlySlow = $input->getOption('slow-only');
        /** @var ?string $selectedCollection */
        $selectedCollection = $input->getOption('collection');
        /** @var int $threshold */
        $threshold = $input->getOption('threshold');

        $this->documentManager = $this->getContainer()->get("doctrine_mongodb")->getManager();
        $metadata = $this->documentManager->getMetadataFactory()->getAllMetadata();

        /** @var ClassMetadataInfo $meta */
        foreach ($metadata as $meta) {
            if ($meta->isEmbeddedDocument === true || $meta->isMappedSuperclass === true) {
                continue;
            }
            $metaIdentifier = (new \ReflectionClass($meta->getName()))->getName();
            $documentsMetadata[$metaIdentifier] = $meta;
        }

        $collectionsOfInterest = $selectedCollection === null ? $documentsMetadata : [$selectedCollection => $documentsMetadata[$selectedCollection]];

        foreach ($collectionsOfInterest as $collection => $meta) {
            if ($meta->customRepositoryClassName === null || class_exists($meta->customRepositoryClassName) === false) {
                continue;
            }

            $class = new \ReflectionClass($meta->customRepositoryClassName);
            $parentClass= $class->getParentClass();
            $documentRepositoryBaseMethods = [];

            if ($parentClass !== false) {
                $documentRepositoryBaseMethods = array_map(
                    function ($method) {
                        return $method->getName();
                    },
                    $parentClass->getMethods(\ReflectionMethod::IS_PUBLIC)
                );
            }

            foreach ($class->getMethods(\ReflectionMethod::IS_PUBLIC) as $repositoryMethod) {
                if (in_array($repositoryMethod->getName(), $documentRepositoryBaseMethods) === true) {
                    continue;
                }
                $testSet[] = [
                    'class' => $class,
                    'instance' => $this->documentManager->getRepository($collection),
                    'method' => $repositoryMethod,
                    'parameters' => $repositoryMethod->getParameters(),
                    'timing' => null,
                    'error' => '',
                ];
            }
        }

        $previousClass = null;
        foreach ($testSet as &$test) {
            if ($previousClass !== $test['class']->getName()) {
                if ($previousClass !== null) {
                    $classTests = array_filter(
                        $testSet,
                        function ($element) use ($previousClass) {
                            return $element['class']->getName() === $previousClass;
                        }
                    );

                    $hasErrors = count(
                        array_filter(
                            $classTests,
                            function ($element) {
                                return $element['error'] !== '';
                            }
                        )
                    ) > 0;

                    if ($hasErrors === true) {
                        $output->write('<error>ERRORS</error>');
                    } else {
                        $output->write('<info>OK</info>');
                    }
                }
                $output->writeln('');
                $output->write($test['class']->getName());
                $output->write('.');
                $previousClass = $test['class']->getName();
            } else {
                $output->write('.');
            }
            try {
                $parameters = $this->prepareParameters($test['parameters'], $test['method']);
                $sw = new Stopwatch();
                $sw->start('test');
                $test['method']->invokeArgs($test['instance'], $parameters);
                $test['timing'] = $sw->stop('test')->getDuration();
            } catch (\Throwable $e) {
                $test['timing'] = -1;
                $test['error'] = $e->getMessage();
            }
        }
        $output->writeln('');

        usort(
            $testSet,
            function ($a, $b) {
                return $a['timing'] > $b['timing'];
            }
        );
        $table = new Table($output);
        $style = new TableStyle();
        $style->setPadType(STR_PAD_LEFT);
        $table->setColumnStyle(2, $style);

        $table->setHeaders(['Repository', 'Method', 'Time (ms)']);
        foreach ($testSet as $test) {
            if ($onlySlow === true && $test['timing'] <= $threshold) {
                continue;
            }

            $table->addRow([$test['class']->getShortName(), $test['method']->getName(), $test['timing']]);
        }

        $table->render();

        $errors = array_filter(
            $testSet,
            function ($element) {
                return $element['error'] != '';
            }
        );

        if ($reportErrors === true && count($errors) > 0) {
            $output->writeln('');
            $output->writeln('Errors:');
            $output->writeln('');
            $table = new Table($output);
            $table->setHeaders(['Method', 'Error']);
            foreach ($errors as $test) {
                $table->addRow([$test['class']->getShortName() . "\n::" . $test['method']->getName(), new TableCell(\wordwrap($test['error']), ['rowspan' => 1])]);
                $table->addRow(new TableSeparator());
            }

            $table->render();
        }
    }

    /**
     * @param array $parameters
     * @param \ReflectionMethod $method
     *
     * @return array
     */
    private function prepareParameters(array $parameters, \ReflectionMethod $method): array
    {
        $type = null;
        $formatted = [];
        $hint = $this->parseDocBlock($method->getDocComment() !== false ? $method->getDocComment() : '');

        foreach ($parameters as $parameter) {
            if ($parameter->hasType() === false) {
                if (in_array($parameter->getName(), $hint) === true) {
                    $type = $hint[$parameter->getName()];
                }
            } else {
                $type = $parameter->getType()->getName();
            }

            $value = $this->generateTypedValue($type);
            $formatted[$parameter->getPosition()] = $value;
        }

        return $formatted;
    }

    /**
     *
     * @param string|null $type
     *
     * @return mixed|null
     */
    private function generateTypedValue(?string $type)
    {
        switch ($type) {
            case 'array':
                return [];
            case 'string':
                return 'aValue';
            case 'int':
                return 0;
            case 'float':
                return 1.0;
            case '\DateTime':
            case 'DateTime':
                return new \DateTime();
            case null:
                return null;
            case 'MongoId':
                return new \MongoId();
            default:
                try {
                    return $this->documentManager->getRepository((string) $type)->findOneBy([]);
                } catch (\Exception $e) {
                    // sd($e->getMessage());
                    return null;
                }
        }
    }

    /**
     * @param string $docBlock
     *
     * @return array
     */
    private function parseDocBlock(string $docBlock): array
    {
        $matches = [];
        $result = [];
        preg_match_all('/@param\s+(?<type>[a-zA-Z\\\\]+)\s+(?<parameter>\$[a-zA-Z]+)/m', $docBlock, $matches, PREG_SET_ORDER, 0);

        foreach ($matches as $match) {
            $result[$match['parameter']] = $match['type'];
        }

        return $result;
    }
}
