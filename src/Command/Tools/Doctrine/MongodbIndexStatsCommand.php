<?php declare (strict_types = 1);

namespace ATS\CoreBundle\Command\Tools\Doctrine;

use ATS\CoreBundle\Service\Util\StringFormatter;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Ali Turki <aturki@ats-digital.com>
 */
class MongodbIndexStatsCommand extends ContainerAwareCommand
{
    const CMD_NAME = 'ats:core:tools:doctrine:mongodb:analyze';
    const COLLECTION_KEYS = ['dcount', 'dsize', 'davgsize', 'icount', 'isize'];
    const INDEX_KEYS = ['collection', 'name', 'count', 'icount', 'isize'];

    const COLLECTION_STATS_CONTEXT = 'collection-stats';
    const INDEX_STATS_CONTEXT = 'index-stats';

    /**
     * @var DocumentManager
     */
    private $documentManager;

    protected function configure()
    {
        $this
            ->setName(self::CMD_NAME)
            ->addOption('collection', null, InputOption::VALUE_REQUIRED, 'Mongo collection to work with')
            ->addOption('index-stats', null, InputOption::VALUE_NONE, 'Prints Index usage stats')
            ->addOption('sort', null, InputOption::VALUE_REQUIRED, 'Sort Key')
            ->addOption('collection-stats', null, InputOption::VALUE_NONE, 'Prints Index usage stats')
            ->setDescription("Prints MongoDB collection index stats");
    }

    /**
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null
     */
    public function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $documentsMetadata = [];
        $singleInheritanceClasses = [];
        $result = [];
        $table = new Table($output);
        $rightAligned = new TableStyle();
        $rightAligned->setPadType(STR_PAD_LEFT);

        /** @var ?string $selectedCollection */
        $selectedCollection = $input->getOption('collection');
        /** @var ?string $sort */
        $sort = $input->getOption('sort');
        /** @var bool $indexStatsOperation */
        $indexStatsOperation = $input->getOption('index-stats');
        /** @var bool $collectionStatsOperation */
        $collectionStatsOperation = $input->getOption('collection-stats');

        $this->documentManager = $this->getContainer()->get("doctrine_mongodb")->getManager();
        $metadata = $this->documentManager->getMetadataFactory()->getAllMetadata();

        /** @var ClassMetadataInfo $meta */
        foreach ($metadata as $meta) {
            $refClass = new \ReflectionClass($meta->getName());
            if ($meta->inheritanceType === ClassMetadataInfo::INHERITANCE_TYPE_SINGLE_COLLECTION) {
                $singleInheritanceClasses[] = $meta->getName();
            }
            if (($refClass->getParentClass() !== false && in_array($refClass->getParentClass()->getName(), $singleInheritanceClasses)) === true ||
                $meta->isEmbeddedDocument === true ||
                $meta->isMappedSuperclass === true ||
                $refClass->isAbstract() === true) {
                continue;
            }

            $metaIdentifier = $refClass->getName();
            $documentsMetadata[$metaIdentifier] = $meta;
        }

        if ($indexStatsOperation === true) {
            $collectionsOfInterest = $selectedCollection === null ? $documentsMetadata : [$selectedCollection => null];
            $result = $this->handleStatsOperation($collectionsOfInterest, $sort);
            $redundantIndex = $this->checkRedundantIndexes($result);

            $table
                ->setHeaders(['Collection', 'Index', 'Usage Count', 'Since'])
                ->setRows($result);

            $table->setColumnStyle(2, $rightAligned);
            $table->render();
            $output->writeln('');
            if (empty($redundantIndex) === false) {
                foreach ($redundantIndex as $msg) {
                    $output->writeln($msg);
                }
                $output->writeln('');
                $output->writeln('Why these indexes are considered redundant ?');
                $output->writeln('https://docs.mongodb.com/manual/core/index-compound/#prefixes');
            }
        }

        if ($collectionStatsOperation === true) {
            $collectionsOfInterest = $selectedCollection === null ? $documentsMetadata : [$selectedCollection => null];
            $result = $this->handleCollectionStats($collectionsOfInterest, $sort ?? 'dcount');

            $table
                ->setHeaders(['Collection', 'Document Count', 'DB Size', '% Size', 'Avg Obj', 'Index Count', 'Index Size'])
                ->setRows($result);

            $table->setColumnStyle(1, $rightAligned);
            $table->setColumnStyle(2, $rightAligned);
            $table->setColumnStyle(3, $rightAligned);
            $table->setColumnStyle(4, $rightAligned);
            $table->setColumnStyle(5, $rightAligned);
            $table->setColumnStyle(6, $rightAligned);
            $table->render();
        }

        return 0;
    }

    /**
     *
     * @param string $collection
     *
     * @return array
     */
    private function getCollectionIndexUsageStats(string $collection): array
    {
        $indexStat = [];
        $builder = $this->documentManager->createAggregationBuilder($collection);

        $result = $builder->indexStats()->execute()->toArray();

        foreach ($result as $indexStatResult) {
            $since = new \DateTime();
            $since->setTimestamp($indexStatResult['accesses']['since']->sec);
            $indexStat[] = [
                'collection' => $collection,
                'name' => $indexStatResult['name'],
                'count' => $indexStatResult['accesses']['ops'],
                'since' => $since->format('Y-m-d H:i:s'),
            ];
        }

        return $indexStat;
    }

    /**
     *
     * @param string $collection
     *
     * @return array
     */
    private function getCollectionStats(string $collection): array
    {
        $mongoDB = new \MongoDB($this->documentManager->getConnection()->getMongoClient(), $this->documentManager->getConfiguration()->getDefaultDB());

        $stats = $mongoDB->command(['collStats' => $this->documentManager->getDocumentCollection($collection)->getName()]);
        $dbSize = $mongoDB->command(['dbStats' => 1])['storageSize'];

        $result = [
            'collection' => $collection,
            // Count
            'dcount' => $stats['count'] ?? 0,
            // DB Size
            'size' => StringFormatter::humanizeMemorySize($stats['storageSize'] ?? 0),
            // Size %
            'psize' => sprintf("%1.2f", 100 * ($stats['storageSize'] ?? 0) / $dbSize),
            // Avg Obj Size
            'avgObjSize' => $stats['avgObjSize'] ?? 0,
            // Indexes Count
            'icount' => $stats['nindexes'] ?? 0,
            // Indexes size
            'isize' => StringFormatter::humanizeMemorySize(array_sum($stats['indexSizes'] ?? [])),
        ];

        return $result;
    }

    /**
     *
     * @param array $collectionsOfInterest
     * @param string|null $sort
     *
     * @return array
     */
    private function handleStatsOperation(array $collectionsOfInterest, ?string $sort): array
    {
        $result = [];
        foreach ($collectionsOfInterest as $collection => $meta) {
            foreach ($this->getCollectionIndexUsageStats($collection) as $indexStat) {
                $result[] = $indexStat;
            }
        }

        return $this->sortResult($result, $sort, self::INDEX_STATS_CONTEXT);
    }

    /**
     *
     * @param array $collectionsOfInterest
     * @param string|null $sort
     *
     * @return array
     */
    private function handleCollectionStats(array $collectionsOfInterest, ?string $sort): array
    {
        $result = [];
        foreach ($collectionsOfInterest as $collection => $meta) {
            $result[] = $this->getCollectionStats($collection);
        }

        return $this->sortResult($result, $sort, self::COLLECTION_STATS_CONTEXT);
    }

    /**
     *
     * @param array $indexes
     *
     * @return array
     */
    private function checkRedundantIndexes(array $indexes): array
    {
        $redundant = [];
        foreach ($indexes as $indexData) {
            foreach ($indexes as $otherIndexData) {
                if ($indexData['collection'] !== $otherIndexData['collection']) {
                    continue;
                }
                if ($indexData['name'] === $otherIndexData['name']) {
                    continue;
                }

                if (strpos($indexData['name'], $otherIndexData['name']) === 0) {
                    $redundant[] = sprintf("Index <comment>[%s]</comment> may be redundant with index <comment>[%s]</comment>", $otherIndexData['name'], $indexData['name']);
                }
            }
        }

        return $redundant;
    }

    /**
     *
     * @param array $result
     * @param string|null $sort
     * @param string $context
     *
     * @return array
     */
    private function sortResult(array $result, ?string $sort, string $context): array
    {
        $keys = $context === self::COLLECTION_STATS_CONTEXT ? self::COLLECTION_KEYS : self::INDEX_KEYS;

        if ($sort !== null) {
            if (in_array($sort, $keys) === false) {
                throw new \Exception("Invalid sort key. Should be one of (" . implode(",", self::COLLECTION_KEYS) . ')');
            } else {
                usort(
                    $result,
                    function ($a, $b) use ($sort) {
                        return $a[$sort] > $b[$sort];
                    }
                );
            }
        }

        return $result;
    }
}
