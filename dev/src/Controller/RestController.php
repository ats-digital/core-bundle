<?php
namespace ATS\CoreBundle\Dev\Controller;

use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use ATS\CoreBundle\Tests\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RestController extends Controller
{
    use RestControllerTrait;

    public function listAction(DocumentManager $dm)
    {
        $users = $dm->getRepository(User::class)->findAll();

        return $this->renderResponse($users, ["small", "profile"]);
    }
}
