<?php

namespace ATS\CoreBundle\Tests\Functional\Manager;

use ATS\CoreBundle\Manager\AbstractManager;
use ATS\CoreBundle\Tests\Document\User;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

class UserManager extends AbstractManager
{
    public function __construct(ManagerRegistry $managerRegistry, $managerName = null)
    {
        parent::__construct($managerRegistry, User::class, $managerName);
    }
}
