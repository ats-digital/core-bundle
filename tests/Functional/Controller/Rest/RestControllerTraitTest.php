<?php

namespace ATS\CoreBundle\Tests\Controller\Rest;

use ATS\CoreBundle\Tests\Document\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use ATS\CoreBundle\Controller\Rest\BaseRestController;
use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use ATS\CoreBundle\Tests\Functional\Manager\UserManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class RestControllerTraitTest extends WebTestCase
{
    use RestControllerTrait;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @uses ATS\CoreBundle\Manager\AbstractManager::__construct
     * @uses ATS\CoreBundle\Manager\AbstractManager::deleteAll
     * @uses ATS\CoreBundle\Manager\AbstractManager::getAll
     * @uses ATS\CoreBundle\Manager\AbstractManager::getDocumentRepository
     * @uses ATS\CoreBundle\Repository\BaseDocumentRepository::deleteAll
     */
    public function testRenderResponse()
    {
        $kernel = self::bootKernel();
        $managerRegistry = $kernel->getContainer()->get('doctrine_mongodb');
        $this->container = $kernel->getContainer();
        $userManager = new UserManager($managerRegistry);
        $userManager->deleteAll();

        $users = [];
        for ($i = 0; $i < 50; $i++) {
            $user = new User("user$i", "user$i@test.com");
            $managerRegistry->getManager()->persist($user);
        }
        $managerRegistry->getManager()->flush();


        $users = $userManager->getAll();

        $response = $this->renderResponse($users);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $response = $this->renderResponse($users, array('undefinedGroup'), Response::HTTP_OK);

        $this->assertEquals(
            "[[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]" ,
                $response->getContent()
        );
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $response = $this->renderResponse(new \Exception());

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }
}
