<?php

namespace ATS\CoreBundle\Tests\Controller;

use ATS\CoreBundle\Tests\Document\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BaseDocumentRepositoryTest extends WebTestCase
{
    protected function setUp()
    {
        $kernel = self::bootKernel();
        $this->managerRegistry = $kernel->getContainer()->get('doctrine_mongodb');
        $this->documentManager = $kernel->getContainer()
            ->get('doctrine_mongodb')
            ->getManager();
    }

    public function testsave()
    {
        $user = new User();
        $user->setUsername("test_username");
        $user->setEmail("user@test.com");
        $user->setEnabled(true);
        $this->documentManager->getRepository(User::class)->save($user);
        $fetched = $this->documentManager->getRepository(User::class)->findOneBy(['username' => 'test_username']);
        $this->assertEquals($fetched->getEmail(), 'user@test.com');
    }

    public function testNoHydrate()
    {
        $user = new User();
        $user->setUsername("test_username");
        $user->setEmail("user@test.com");
        $user->setEnabled(true);
        $this->documentManager->getRepository(User::class)->save($user);
        $fetched = $this->documentManager->getRepository(User::class)->noHydrate(['username' => 'test_username'], ['username', 'email']);
        $this->assertInternalType('array', $fetched[0]);
        $this->assertArrayHasKey('username', $fetched[0]);
        $this->assertArrayHasKey('email', $fetched[0]);
        $this->assertArrayNotHasKey('enabled', $fetched[0]);
    }

    public function testDelete()
    {
        $this->documentManager->getRepository(User::class)->deleteAll();
        $user = new User();
        $user->setUsername("test_username");
        $user->setEmail("user@test.com");
        $user->setEnabled(true);
        $this->documentManager->getRepository(User::class)->save($user);
        $fetched = $this->documentManager->getRepository(User::class)->findOneBy(['username' => 'test_username']);
        $this->assertEquals($fetched->getEmail(), 'user@test.com');
        $this->documentManager->getRepository(User::class)->delete($user);

        $all = $this->documentManager->getRepository(User::class)->findAll();

        $this->assertCount(0, $all);
    }

    public function testLike()
    {
        $this->documentManager->getRepository(User::class)->deleteAll();
        $user = new User();
        $user->setUsername("test_username");
        $user->setEmail("user@test.com");
        $user->setEnabled(true);
        $this->documentManager->getRepository(User::class)->save($user);
        $user = new User();
        $user->setUsername("toto");
        $user->setEmail("toto@test.com");
        $user->setEnabled(true);
        $this->documentManager->getRepository(User::class)->save($user);

        // Ensure indexes are created
        $this->documentManager
            ->getConnection()
            ->getMongoClient()
            ->selectDB('core_test')
            ->selectCollection('User')
            ->createIndex(['username' => 'text']);

        $searched = $this->documentManager->getRepository(User::class)->textSearch('test_username');
        $this->assertEquals($searched[0]->getUsername(), 'test_username');
    }

    public function testCount()
    {
        $this->documentManager->getRepository(User::class)->deleteAll();
        for($i = 0; $i < 50; $i++) {
            $user = new User();
            $user->setUsername($i);
            $user->setEmail("$i@test.com");
            if ($i < 30) {
                $user->setEnabled(true);
            } else {
                $user->setEnabled(false);
            }
            $this->documentManager->persist($user);
        }
        $this->documentManager->flush();

        $this->assertEquals(50, $this->documentManager->getRepository(User::class)->count());
        $this->assertEquals(30, $this->documentManager->getRepository(User::class)->count(['enabled' => true]));
        $this->assertEquals(20, $this->documentManager->getRepository(User::class)->count(['enabled' => false]));
    }
}
