<?php

namespace ATS\CoreBundle\Tests\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @ODM\Document(repositoryClass="ATS\CoreBundle\Repository\BaseDocumentRepository")
 * @ODM\Index(keys={"username"="text"})
 */
class User
{
    /**
     * @ODM\Id(strategy="auto")
     * @Groups({"all", "small"})
     */
    private $id;
    /**
     * @ODM\Field(type="string")
     * @Groups({"all", "profile"})
     */
    private $username;

    /**
     * @ODM\Field(type="string", nullable="false")
     * @Groups({"all"})
     */
    private $email;

    /**
     * @ODM\Field(type="bool")
     * @Groups({"all"})
     */
    private $enabled;

    /**
     * @ODM\Field(type="date")
     * @Groups({"all"})
     */
    private $lastLogin;

    /**
     * @ODM\Field(type="collection")
     * @Groups({"all"})
     */
    private $roles;

    public function __construct($username = '', $email = '')
    {
        $this->username = $username;
        $this->email = $email;
    }
    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of enabled
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set the value of enabled
     *
     * @return  self
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get the value of lastLogin
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set the value of lastLogin
     *
     * @return  self
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get the value of roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set the value of roles
     *
     * @return  self
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }
}
