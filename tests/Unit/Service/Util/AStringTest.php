<?php

namespace ATS\CoreBundle\Tests\Service\Util;

use ATS\CoreBundle\Service\Util\AString;
use ATS\CoreBundle\Tests\Document\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AStringTest extends KernelTestCase
{
    public function testStartsWith()
    {
        $str = "Hello World";
        $wrapper = new AString($str);

        $this->assertEquals(true, $wrapper->startsWith("Hello"));
        $this->assertEquals(true, $wrapper->startsWith("Hello", true));
        $this->assertEquals(false, $wrapper->startsWith("hello", false));
        $this->assertEquals(false, $wrapper->startsWith("Not Hello", true));
        $this->assertEquals(false, $wrapper->startsWith("Not Hello", false));
    }

    public function testEndsWith()
    {
        $str = "Hello World";
        $wrapper = new AString($str);

        $this->assertEquals(true, $wrapper->endsWith("World"));
        $this->assertEquals(true, $wrapper->endsWith(" World", true));
        $this->assertEquals(false, $wrapper->endsWith("world", false));
        $this->assertEquals(false, $wrapper->endsWith("gibberish", true));
        $this->assertEquals(false, $wrapper->endsWith("gibberish", false));
    }

    public function testContains()
    {
        $str = "Hello World";
        $wrapper = new AString($str);

        $this->assertEquals(true, $wrapper->contains("lo Wo"));
        $this->assertEquals(true, $wrapper->contains("lo Wo", true));
        $this->assertEquals(true, $wrapper->contains("lo Wo", false));
        $this->assertEquals(false, $wrapper->contains(" lo Wo ", true));
        $this->assertEquals(false, $wrapper->contains(" lo Wo ", false));
    }

    public function testStringify()
    {
        $kernel = self::bootKernel();
        $documentManager = $kernel->getContainer()->get('doctrine_mongodb')->getManager();

        $this->assertEquals('True', AString::stringify(true));
        $this->assertEquals('False', AString::stringify(false));
        $this->assertEquals('1|2|3', AString::stringify([1, 2, 3]));
        $this->assertEquals('', AString::stringify([]));
        $this->assertEquals('2001-01-01', AString::stringify((new \DateTime('2001-01-01'))));
        $object = new \StdClass();
        $this->assertEquals("stdClass Object", AString::stringify($object));
        $user = new User("username", "email");
        $documentManager->persist($user);
        $id = $user->getId();
        $this->assertEquals("$id", AString::stringify($user));
    }

    public function testRandom()
    {
        $this->assertEquals(32, strlen(AString::random(32)));
    }

    public function testRightOf()
    {
        $str = new AString("Hello pretty world");

        $this->assertEquals(" world", (string) $str->rightOf("pretty"));
        $this->assertEquals("", (string) $str->rightOf("world"));
        $this->assertEquals(" pretty world", (string) $str->rightOf("Hello"));
        $this->assertEquals("", (string) $str->rightOf("TOTO"));
        $this->assertEquals("Hello pretty world", (string) $str->rightOf(""));
    }

    public function testLeftOf()
    {
        $str = new AString("Hello pretty world");

        $this->assertEquals("Hello ", (string) $str->leftOf("pretty"));
        $this->assertEquals("Hello pretty ", (string) $str->leftOf("world"));
        $this->assertEquals("", (string) $str->leftOf("Hello"));
        $this->assertEquals("", (string) $str->leftOf("TOTO"));
        $this->assertEquals("Hello pretty world", (string) $str->leftOf(""));
    }

    public function testTrim()
    {
        $str = new AString("    Hello    ");
        $this->assertEquals("Hello", (string) $str->trim());
        $str = new AString("Hello    ");
        $this->assertEquals("Hello", (string) $str->trim());
        $str = new AString("    Hello");
        $this->assertEquals("Hello", (string) $str->trim());
        $str = new AString("Hello");
        $this->assertEquals("Hello", (string) $str->trim());
        $str = new AString("");
        $this->assertEquals("", $str->trim());
    }

    public function testPosition()
    {
        $str = new AString("hello there");

        $this->assertEquals(6, $str->position("there"));
        $this->assertEquals(-1, $str->position("toto"));
    }

    public function testCapitalize()
    {
        $str = new AString("hello there");
        $this->assertEquals("Hello There", $str->capitalize());

        $str = new AString("");
        $this->assertEquals("", $str->capitalize());

        $str = new AString("HELLO THERE");
        $this->assertEquals("Hello There", $str->capitalize());

        $str = new AString("Hello There");
        $this->assertEquals("Hello There", $str->capitalize());
    }

    public function testToUpper()
    {
        $str = new AString("hello there");
        $this->assertEquals("HELLO THERE", $str->toUpper());

        $str = new AString("HELLO THERE");
        $this->assertEquals("HELLO THERE", $str->toUpper());

        $str = new AString("");
        $this->assertEquals("", $str->toUpper());
    }

    public function testToLower()
    {
        $str = new AString("hello there");
        $this->assertEquals("hello there", $str->toLower());

        $str = new AString("HELLO THERE");
        $this->assertEquals("hello there", $str->toLower());

        $str = new AString("");
        $this->assertEquals("", $str->toLower());
    }

    public function testLength()
    {
        $str = new AString("hello there");
        $this->assertEquals(11, $str->length());
        $str = new AString("");
        $this->assertEquals(0, $str->length());
    }

    public function testReverse()
    {
        $str = new AString("hello there");
        $this->assertEquals("ereht olleh", (string) $str->reverse());
    }

    public function testSplit()
    {
        $str = new AString("Hello, is there any body in there");
        $arr = $str->split(',');

        $this->assertEquals("Hello", $arr[0]);
        $this->assertEquals(" is there any body in there", $arr[1]);
        $this->assertCount(2, $arr);
    }

    public function testChaining()
    {
        $str = new AString("   ATS is awsome, you should join us some day. We do great stuff !!   ");

        $str->capitalize()->trim()->reverse();

        $this->assertEquals('!! ffutS taerG oD eW .yaD emoS sU nioJ dluohS uoY ,emoswA sI stA', $str->__toString());
    }

    public function testAsArray()
    {
        $str = new AString("Hello World");
        $arr = $str->asArray();

        $this->assertCount(11, $arr);

        $this->assertInternalType('array', $arr);
    }

    public function testOffsetGet()
    {
        $str = new AString("Hello World");

        $this->assertEquals('H', $str[0]);
        $this->assertEquals('d', $str[-1]);

        $this->expectException('OutOfBoundsException');
        $undefined = $str[100];
    }

    public function testOffsetExists()
    {
        $str = new AString("Hello World");

        $this->assertTrue($str->offsetExists(0));
        $this->assertTrue($str->offsetExists(-1));
        $this->assertFalse($str->offsetExists(100));
    }

    public function testOffsetSet()
    {
        $str = new AString("Hello World");
        $this->expectException('Exception');
        $str[0] = 'A';
    }

    public function testOffsetUnset()
    {
        $str = new AString("Hello World");
        $this->expectException('Exception');
        unset($str[1]);
    }

    public function testIterator()
    {
        $str = new AString("AAA");
        foreach ($str as $char) {
            $this->assertEquals('A', $char);
        }
    }

    public function testAppend()
    {
        $str = new AString("AAA");

        $this->assertEquals("AAABBB", $str->append("BBB"));
    }

    public function testPrepend()
    {
        $str = new AString("AAA");

        $this->assertEquals("BBBAAA", $str->prepend("BBB"));
    }

    public function testReplace()
    {
        $str = new AString("AAA");
        $this->assertEquals("BBB", (string)$str->replace("AAA", "BBB"));
    }
}
