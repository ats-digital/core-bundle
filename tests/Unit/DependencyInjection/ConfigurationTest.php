<?php declare(strict_types=1);

namespace ATS\CoreBundle\Tests\Unit\DependencyIntection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use ATS\CoreBundle\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;

/**
 * ConfigurationTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ConfigurationTest extends TestCase
{
    /**
     * Test suits for getConfigTreeBuilder
     *
     * @covers ATS\CoreBundle\DependencyInjection\Configuration::getConfigTreeBuilder
     */
    public function testGgetConfigTreeBuilder()
    {
        $configuration = new Configuration();
        $this->assertInstanceOf(TreeBuilder::class, $configuration->getConfigTreeBuilder());
    }
}
